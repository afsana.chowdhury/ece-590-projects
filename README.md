# ECE 590 Projects
_Language: Python_

**Project 1:**
Implementation of five sorting algorithms:
1) Selection sort
2) Insertion sort
3) Bubble sort
4) Merge sort
5) Quick sort

**Project 2:**
Depth First Search (DFS) & Breadth First Search (BFS) for solving Maze


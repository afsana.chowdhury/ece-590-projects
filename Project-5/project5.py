"""
ECE 590
Project 5
Fall 2021

Partner 1: Afsana Chowdhury (ahc35)
Partner 2: N/A
Date: 12/03/2021

Program: MST/TSP

Description: This program implements both Prim’s and Kruskal’s algorithms
for finding a Minimum Spanning Tree (MST), and then uses that MST to
approximate the solution for the Traveling Salesman Problem (TSP).
"""

# Import math, itertools, and time.
import math
import itertools
import time
import random

# Import the Priority Queue.
from p5priorityQueue import *

################################################################################

"""
Function: prim

Input: 1) adjList 2) adjMat
Output: None

Description: This function implements Prim's Algorithm to find
the MST of the input graph. By the end of this function,
every vertex has been assigned the proper vertex.prev values.
"""
def prim(adjList, adjMat):
    initGraph(adjList)

    # Make the priority queue using cost for sorting.
    # Note: The Vertex class has an overloaded less-than (<) operator for use
    # in the priority queue.
    Q = PriorityQueue(adjList)

    while not Q.isEmpty():
    	# Get the next unvisited vertex and visit it.
    	v = Q.deleteMin()
    	v.visited = True

    	# For each edge out of v.
    	for neighbor in v.neigh:
    		# If the edge leads out, update.
    		if not neighbor.visited:
    			length = adjMat[v.rank][neighbor.rank]
    			if neighbor.cost > length:
    				neighbor.cost = length
    				neighbor.prev = v
    return

"""
Function: initGraph

Input: adjList

Description: Sets initial cost and prev for every vertex in the graph.
Also sets the randomly selected "start" vertex's dist to 0.
"""
def initGraph(adjList):
	# Initialize all costs to infinity and prev to null.
	for vertex in adjList:
		vertex.cost = math.inf
		vertex.prev = None
	# Pick an arbitrary start vertex and set cost to 0.
	startVertex = random.randint(0, len(adjList)-1)
	start = adjList[startVertex]
	start.cost = 0
	return
################################################################################

"""
Function: kruskal

Input: 1) adjList 2) adjMat
Output: A list of edges that are in the MST

Description: This function implements Kruskal's Algorithm to find
the MST of the input graph.

Note: the edgeList is ALREADY SORTED!
Note: Use the isEqual method of the Vertex class when comparing vertices.
"""
def kruskal(adjList, edgeList):
    # Initialize all singleton sets for each vertex.
    for vertex in adjList:
    	makeset(vertex)

    # Initialize the empty MST.
    X = []

    # Typically we would sort the edges by weight at this point.
    # But in this case, we are told that the edgeList is already sorted

    # Loop through the edges in increasing order.
    for e in edgeList:
    	# If the min edge crosses a cut, add it to our MST.
    	u, v = e.vertices
    	if find(u) != find(v):
    		X.append(e)
    		union(u,v)
    
    return X

################################################################################

"""
Disjoint Set Functions:
    makeset
    find
    union

These functions will operate directly on the input vertex objects.
"""

"""
makeset: this function will create a singleton set with root v.
"""
def makeset(v):
    v.pi = v
    v.height = 0
    return

"""
find: this function will return the root of the set that contains v.
Note: we will use path compression here.

"""
def find(v):
    # If we are not at the root.
    if v != v.pi:
    	# Set our parent to be the root,
    	# which is also the root of our parent!
    	v.pi = find(v.pi)

    # Return the root, which is now our parent!
    return v.pi
    
"""
union: this function will union the sets of vertices v and u.
"""
def union(u,v):
    # First, find the root of the tree for u
    # and the tree for v.
    ru = find(u)
    rv = find(v)
    
    # If the sets are already the same, return.
    if ru.isEqual(rv):
    	return

    # Make shorter set point to taller set.
    if ru.height > rv.height:
    	rv.pi = ru
    elif ru.height < rv.height:
    	ru.pi = rv
    else:
    	# Same height, break tie.
    	ru.pi = rv
    	# Tree got taller, increment rv.height.
    	rv.height += 1
    
    return

################################################################################

"""
Function: tsp

Input: 1) adjList 2) start (vertex object)
Output: The tour array of vertex ranks.

Description: This function traces the TSP tour using depth first search (DFS)
on the MST.
"""
def tsp(adjList, start):
	# create empty tour
	tour = []

	# set all the vertices as unvisited
	for vertex in adjList:
		vertex.visited = False
	
	# create stack for DFS
	verticesToVisit = [];

	# visit "start" vertex
	start.visited = True
	verticesToVisit.append(start)

	# while stack is not empty, visit vertices
	while len(verticesToVisit) != 0:
		currentVertex = verticesToVisit.pop()
		
		# add the vertex to tour if it's not in the list already
		if not (currentVertex.rank in tour):
			tour.append(currentVertex.rank)

		# for each neighbor in MST
		for neigh in currentVertex.mstN:
			if not neigh.visited:
				neigh.visited = True
				verticesToVisit.append(neigh)

	# finally add the start vertex to complete the cycle
	tour.append(start.rank)
	return tour

################################################################################

# Import the tests (since we have now defined prim and kruskal).
from p5tests import *

"""
Main function.
"""
if __name__ == "__main__":
    verb = False # Set to true for more printed info.
    print('Testing Prim\n')
    print(testMaps(prim, verb))
    print('\nTesting Kruskal\n')
    print(testMaps(kruskal, verb))

"""
ECE 590
Fall 2021
Project 1 : Sorting

Partner 1: Afsana Chowdhury (ahc35)
Partner 2: N/A
Date: 10/29/2021

Program Description: 
Implementation of the following five algorithms:
  1. Selection Sort
  2. Insertion Sort
  3. Bubble Sort
  4. Merge Sort
  5. Quick Sort

Note: Please refer to the project report for the comparison of the runtimes.
"""

"""
Funtion Name: SelectionSort
Input: listToSort (sorted/unsorted)
Output: Sorted listToSort

Brief Description:
- Main idea: Separate the array into a sorted and an unsorted component
- Starting from the first element, find the lowest element of the array
- Swap the lowest element with the element at current index
- Move to the next element
- Continue until we reach the end of the array
"""
def SelectionSort(listToSort):
    # starting from 0, move the ref i.e. reference point
    for ref in range(len(listToSort)):
      # starting from the ref, find the lowest value
      for i in range(ref, len(listToSort)):
         if(listToSort[i] < listToSort[ref]):
            # swap
            listToSort[i], listToSort[ref] = listToSort[ref], listToSort[i]
            pass
         pass
      pass
    return listToSort

"""
Function Name: InsertionSort
Input: listToSort (sorted/unsorted)
Output: Sorted listToSort

Brief Description:
- Main idea: Separate the array into a sorted and an unsorted component
- Starting from the first element, use the element at current index as a key
- Take the next element and search backwards starting from the key,
  and bubble the element to the correct position
- Move to the next element after current key and continue
  until we reach the end of the array
"""
def InsertionSort(listToSort):
    # starting from 0, move the ref i.e. reference point
    for ref in range(len(listToSort) - 1):
      k = ref + 1
      key = listToSort[k]
      # starting from the ref + 1, go backwards
      while k > 0 and key < listToSort[k - 1]:
        listToSort[k] = listToSort[k - 1]
        k -= 1
        pass
      listToSort[k] = key
      pass
    return listToSort

"""
Function Name: BubbleSort
Input: listToSort (sorted/unsorted)
Output: Sorted listToSort

Brief Description:
- Iterate through the array
- Compare every two adjacent elements
- If they are out of order, swap them
- Repeat until no more swaps are made
"""
def BubbleSort(listToSort):
    lastElement = len(listToSort) - 1
    somethingSwapped = True
    while somethingSwapped and lastElement > 0:
      somethingSwapped = False
      for k in range(lastElement):
        if listToSort[k] > listToSort[k + 1]:
          #swap
          listToSort[k], listToSort[k + 1] = listToSort[k + 1], listToSort[k]
          somethingSwapped = True
          pass
        pass
      lastElement -= 1
      pass
    return listToSort

"""
Function Name: MergeSort
Input: listToSort (sorted/unsorted)
Output: Sorted listToSort

Brief Description:
- Main idea: Divide & Conquer Approach
- Split the array into half and recurcsively sort each half
- Merging strategy:
    - Compare the smallest elements
    - Remove the smaller of the two and insert in the merged list
"""
def MergeSort(listToSort):
    if len(listToSort) == 1 :
      return listToSort

    elif len(listToSort) == 2 :
      if listToSort[1] < listToSort[0] :
        listToSort[0], listToSort[1] = listToSort[1], listToSort[0]
        pass
      return listToSort

    else :
      # split the list
      mid = len(listToSort) // 2
      splitList_Left = listToSort[:mid]
      splitList_Right = listToSort[mid:]

      # recursive call with two halves
      MergeSort(splitList_Left)
      MergeSort(splitList_Right)

      # merge the sorted halves
      index_mergedList = 0
      index_LeftList = 0
      index_RightList = 0

      # compare and copy elements into listToSort
      # until we reach the end at any half
      while index_LeftList < len(splitList_Left) and index_RightList < len(splitList_Right) :
        
        if splitList_Left[index_LeftList] < splitList_Right[index_RightList] :
          listToSort[index_mergedList] = splitList_Left[index_LeftList]
          index_mergedList += 1
          index_LeftList += 1

        elif splitList_Right[index_RightList] < splitList_Left[index_LeftList] :
          listToSort[index_mergedList] = splitList_Right[index_RightList]
          index_mergedList += 1
          index_RightList += 1

        else : # both elements are equal, so we can copy both
          listToSort[index_mergedList] = splitList_Left[index_LeftList]
          index_mergedList += 1
          index_LeftList += 1
          listToSort[index_mergedList] = splitList_Right[index_RightList]
          index_mergedList += 1
          index_RightList += 1
          pass

        pass

      # now check if there is any element left in any of the halves
      while index_LeftList < len(splitList_Left) :
        listToSort[index_mergedList] = splitList_Left[index_LeftList]
        index_mergedList += 1
        index_LeftList += 1
        pass

      while index_RightList < len(splitList_Right) :
        listToSort[index_mergedList] = splitList_Right[index_RightList]
        index_mergedList += 1
        index_RightList += 1
        pass
      pass # end of if-else
    return listToSort

"""
Function Name: QuickSort
Input: 
  1. listToSort (sorted/unsorted)
  2. Optional: i (starting index). 0 if not specified
  3. Optional: j (one past the last index). len(listToSort) is not specified
Output: Sorted listToSort

Brief Description:
- Main idea: Divide & Conquer Approach
- Choose a pivot (We picked a random element in the list)
- Partition the list based on the pivot
    - everthing smaller than or equal to the in front
    - everything greater than or equal to the pivot in back
- Recurse on each partition
"""
def QuickSort(listToSort, i=0, j=None):
    # Set default value for j if None.
    if j == None:
        j = len(listToSort)

    # set the begin and end pointers
    front_index = i
    back_index = j - 1 # j points one past the end of the list

    if front_index >= back_index : # only one element OR empty
      return listToSort # nothing to sort

    elif (back_index - front_index) == 1 : # only two elements
      if listToSort[back_index] < listToSort[front_index] :
        # swap
        listToSort[front_index], listToSort[back_index] = listToSort[back_index], listToSort[front_index]
        pass
      return listToSort
  
      pass # end of outer if-else

    #choose pivot
    pivot_index = random.randrange(i, j)
    pivot = listToSort[pivot_index]

    front_stuck = False
    back_stuck = False

    while front_index < back_index :
      # Check the front and make sure it's less than or equal to the pivot
      # No need to check if front_index is stuck still
      if not front_stuck and listToSort[front_index] <= pivot :
        front_index += 1
      else :
        front_stuck = True
        pass

      # Now check the back and make sure it's strictly greater than the pivot
      # Note we don't want it to be same as pivot at this point

      if not back_stuck and listToSort[back_index] > pivot : 
        back_index -= 1
      else :
        back_stuck = True
        pass

      if front_stuck and back_stuck : # if none could move
        #swap and move
        listToSort[front_index], listToSort[back_index] = listToSort[back_index], listToSort[front_index]
        front_stuck = False
        back_stuck = False
        front_index += 1
        back_index -= 1
        pass

      pass # end of while-loop

    # split the list
    if front_index == back_index :
      if listToSort[front_index] < pivot : 
        # avoiding '=' for corner case where pivot is the largest element and
        # the front and back index end up at the last element
        split_index = front_index
      else :
        split_index = front_index - 1
        pass
    else : # front_index > back_index
      # note front_index < back_index is not possible after completion of while-loop
      split_index = back_index
      pass

    # recursive calls with split lists
    QuickSort(listToSort, i, split_index + 1)
    QuickSort(listToSort, split_index + 1, j)

    return listToSort

"""
Importing the testing code after function defs to ensure same references.
"""
from project1tests import *

"""
Main function.
"""
if __name__ == "__main__":
    print('Testing Selection Sort')
    print()
    testingSuite(SelectionSort)
    print()
    print('Testing Insertion Sort')
    print()
    testingSuite(InsertionSort)
    print()
    print('Testing Bubble Sort')
    print()
    testingSuite(BubbleSort)
    print()
    print('Testing Merge Sort')
    print()
    testingSuite(MergeSort)
    print()
    print('Testing Quick Sort')
    print()
    testingSuite(QuickSort)
    print()
    print('UNSORTED measureTime')
    print()
    measureTime()
    print()
    print('SORTED measureTime')
    print()
    measureTime(True)

"""
ECE 590
Project 2
Fall 2021

project2.py

Partner 1: Afsana Chowdhury (ahc35)
Partner 2: N/A
Date: 11/3/2021

Program: Depth First Search (DFS) &
		 Breadth First Search (BFS)
		 for solving Maze
"""

# Import math and other p2 files.
import math
from p2tests import *

"""
BFS/DFS function

INPUTS
maze: A Maze object representing the maze.
alg:  A string that is either 'BFS' or 'DFS'.

OUTPUTS
path: The path from maze.start to maze.exit.
"""
def bdfs(maze, alg):
    # If the alg is not BFS or DFS, raise exception.
    if (alg != 'BFS') and (alg != 'DFS'):
        raise Exception('Incorrect alg! Need BFS or DFS!')

    elif alg == 'DFS' :
    	dfs(maze)
    else : # alg == 'BFS' 
    	bfs(maze)

    return maze.path

"""
DFS function

INPUT
maze: A Maze object representing the maze

DESCRIPTION
This function performs Depth First Search (DFS)
on the input maze to solve it and changes the "path"
member of maze with the path from maze.start to maze.exit.
"""
def dfs(maze):
    # Step-1: Initialize maze
    resetMaze(maze)
    verticesToVist = Stack()

    # Step-2: Vist Start vertex
    startVertex = maze.start
    startVertex.visited = True;
    verticesToVist.push(startVertex)

    # Step-3: while stack is not empty, visit vertices
    while not verticesToVist.isEmpty() :
    	currentVertex = verticesToVist.pop()
    	if currentVertex.isEqual(maze.exit) :
    		break # found the exit!!!
    	for x in range (0, len(currentVertex.neigh)) : # for each neighbour
    		neighbour = currentVertex.neigh[x]
    		if not neighbour.visited :
    			neighbour.visited = True
    			neighbour.prev = currentVertex
    			verticesToVist.push(neighbour)


    # Step-4: create the path from start to exit
    pathStack = Stack()
    # push the exit vertex first
    currentVertex = maze.exit
    while currentVertex.prev != None : # until the start vertex
    	pathStack.push(currentVertex.rank)
    	currentVertex = currentVertex.prev

    # push the start vertex
    pathStack.push(currentVertex.rank)

    # pathStack is from exit to start
    # so if we pop now, it'll be reversed
    # i.e. from start to exit
    while not pathStack.isEmpty() :
    	maze.path.append(pathStack.pop())

    return

"""
BFS function

INPUT
maze: A Maze object representing the maze

DESCRIPTION
This function performs Breadth First Search (BFS)
on the input maze to solve it and changes the "path"
member of maze with the path from maze.start to maze.exit.
"""
def bfs(maze) :
	# Step-1: Initialize maze
    resetMaze(maze)
    verticesToVist = Queue()

    # Step-2: Vist Start vertex
    startVertex = maze.start
    startVertex.visited = True;
    verticesToVist.push(startVertex)

    # Step-3: while queue is not empty, visit vertices
    while not verticesToVist.isEmpty() :
    	currentVertex = verticesToVist.pop()
    	if currentVertex.isEqual(maze.exit) :
    		break # found the exit!!!
    	for x in range (0, len(currentVertex.neigh)) : # for each neighbour
    		neighbour = currentVertex.neigh[x]
    		if not neighbour.visited :
    			neighbour.visited = True
    			neighbour.prev = currentVertex
    			verticesToVist.push(neighbour)

    # Step-4: create the path from start to exit
    pathStack = Stack()
    # push the exit vertex first
    currentVertex = maze.exit
    while currentVertex.prev != None : # until the start vertex
    	pathStack.push(currentVertex.rank)
    	currentVertex = currentVertex.prev

    # push the start vertex
    pathStack.push(currentVertex.rank)

    # pathStack is from exit to start
    # so if we pop now, it'll be reversed
    # i.e. from start to exit
    while not pathStack.isEmpty() :
    	maze.path.append(pathStack.pop())

    return

"""
resetMaze function for resetting "visited" and "prev"
members in Maze class
"""
def resetMaze(maze):
	for vertex in maze.adjList:
		vertex.dist = math.inf
		vertex.visited = False
		vertex.prev = None
	maze.path = []
	return

"""
Main function.
"""
if __name__ == "__main__":
    testMazes(False)
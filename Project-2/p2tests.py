"""
Math 560
Project 2
Fall 2021

p2tests.py
"""

# Import math and other p2 files.
import math
from p2stack import *
from p2queue import *
from p2maze import *

"""
testMazes function will test all of the mazes.
"""
def testMazes(verbosity=False):
    m = Maze(0,verbosity)
    print('Testing Maze 0, DFS')
    m.solve('DFS',verbosity,False)
    print('Testing Maze 0, BFS')
    m.solve('BFS',verbosity,False)
    m = Maze(1,verbosity)
    print('Testing Maze 1, DFS')
    m.solve('DFS',verbosity,False)
    print('Testing Maze 1, BFS')
    m.solve('BFS',verbosity,False)
    m = Maze(2,verbosity)
    print('Testing Maze 2, DFS')
    m.solve('DFS',verbosity,False)
    print('Testing Maze 2, BFS')
    m.solve('BFS',verbosity,False)
    m = Maze(3,verbosity)
    print('Testing Maze 3, DFS')
    m.solve('DFS',verbosity,False)
    print('Testing Maze 3, BFS')
    m.solve('BFS',verbosity,False)
    m = Maze(4,verbosity)
    print('Testing Maze 4, DFS')
    m.solve('DFS',verbosity,False)
    print('Testing Maze 4, BFS')
    m.solve('BFS',verbosity,False)
    plt.show()
    return

def testStack() :
    newStack = Stack()
    newStack.push(3)
    newStack.push(4)
    newStack.pop()
    newStack.push(0)
    newStack.push(10)
    newStack.push(15)
    print(newStack)
    return

def testQueue() :
    newQueue = Queue()
    newQueue.push(3)
    newQueue.push(4)
    newQueue.pop()
    newQueue.push(0)
    newQueue.push(10)
    newQueue.push(15)
    newQueue.pop()
    newQueue.pop()
    newQueue.push(6)
    newQueue.push(1)
    newQueue.push(7)
    newQueue.pop()
    newQueue.push(5)
    newQueue.push(12)
    newQueue.push(13)
    print(newQueue)
    return

################################################################################

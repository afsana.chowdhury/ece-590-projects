"""
ECE 590
Project 4: Edit Distance
Fall 2021

Partner 1: Afsana Chowdhury (ahc35)
Partner 2: N/A
Date: 11/18/2021

Program Description:
Implementing the Edit Distance algorithm for two strings to determine
the number of edits required to convert one string into the other
"""

# Import p4tests.
from p4tests import *

################################################################################

class dpTableElement:

    """
    Class attributes:
    
    edit    # The number of edits done so far.
    prev    # The previous element, i.e. where this change came from
    		  Options for prev:
        		L: insert the letter for this column
        		U: delete the letter for this row
        		D (+1): change letter for this row to the one for this column
        		D (+0): match, no edit required
    """

    """
    __init__ function to initialize the dpTableElement.
    """
    def __init__(self, editNum = 0, previous = 'None'):

        self.edit = editNum  # Default: No edits so far
        self.prev = previous # Default: No previous element

        return

        """
    __repr__ function to print a dpTableElement
    """
    def __repr__(self):
        return '%d, %s' % (self.edit, self.prev)

"""
Function: findMin

Inputs: 
Following three objects which have number of edits
for the three different cases:
	1. insert 2. delete 3. substitute

Outputs:
The minimum of the three inputs

Description: 
This function checks which operation among insert, delete and
substitute requires minimum number of edits
"""
def findMin(insert, delete, substitute):
	minimum = insert.edit
	direction = insert.prev
	if delete.edit < minimum:
		minimum = delete.edit
		direction = delete.prev
	elif substitute.edit < minimum:
		minimum = substitute.edit
		direction = substitute.prev

	return minimum, direction

"""
Function: ED

Inputs: 
1. src: source string to be changed
2. dest: destination string to match with
3. prob (optional)

Output:
1. dist: The number of edits required to convert src into dest
where edits can be insertions, deletions, or substitutions.
2. edits: the list of edits to perform.

Description: 
This function uses a dynamic programming approach to convert
src string to dest string, and then reconstructs an optimal
set of edits using the DP table.
"""
def ED(src, dest, prob='ED'):
    # Check the problem to ensure it is a valid choice.
    if (prob != 'ED') and (prob != 'ASM'):
        raise Exception('Invalid problem choice!')

    # First initialize the DP table
    n = len(src) # num of rows -> i
    m = len(dest) # num of columns -> j

    dpTable = [[dpTableElement() for j in range(m+1)] for i in range(n+1)]
    # for index (i,j), the format of index in dpTable is:
    # [[ ( 0, 0), ( 0, 1), ... ( 0, m)],
    #  [ ( 1, 0), ( 1, 1), ... ( 1, m)],
    #  ... ... ...
    #  [ ( n, 0), ( n, 1), ... ( n, m)]]

    # Fill in the base cases (first row and first column)
    for i in range(n+1):
    	# first column: j=0 i.e. no characters left in dest
    	# delete the remaining characters in src
    	(dpTable[i][0]).edit = i
    	(dpTable[i][0]).prev = 'U'

    if prob != 'ASM' : # For ASM, leaving first row at 0
	    for j in range(m+1):
    		# first row: i=0 i.e. no characters left in src
    		# insert the remaining characters from dest
    		(dpTable[0][j]).edit = j
    		(dpTable[0][j]).prev = 'L'

    # Fill in the table by iterating across each row.
    for i in range(1,n+1):
    	for j in range(1,m+1):
    		if src[i-1] == dest[j-1]:
    			(dpTable[i][j]).edit = (dpTable[i-1][j-1]).edit
    			(dpTable[i][j]).prev = 'D'

    		else:
    			insert = dpTableElement(1 + (dpTable[i][j-1]).edit, 'L')
    			delete = dpTableElement(1 + (dpTable[i-1][j]).edit, 'U')
    			substitute = dpTableElement(1 + (dpTable[i-1][j-1]).edit, 'D')
    			(dpTable[i][j]).edit, (dpTable[i][j]).prev = findMin(insert, delete, substitute)

    # Reconstruct the solution
    i = n #rows
    j = m #columns
    dist = dpTable[i][j].edit
    edits = []

    while dpTable[i][j].edit != 0:
    	if dpTable[i][j].prev == 'L':
    		changeType = 'insert'
    		destChar = dest[j-1]
    		srcIndex = i
    		j = j-1 # move left

    	elif dpTable[i][j].prev == 'U':
    		changeType = 'delete'
    		destChar = src[i-1]
    		srcIndex = i-1
    		i = i-1 # move up

    	elif dpTable[i][j].prev == 'D':
    		if dpTable[i][j].edit == dpTable[i-1][j-1].edit:
    			changeType = 'match'
    		else :
    			changeType = 'sub'
    		destChar = dest[j-1]
    		srcIndex = i-1
    		i, j = i-1, j-1 # move diagonally

    	# add the 3-tuple to the list of edits
    	edits.append((changeType, destChar, srcIndex))

    return dist, edits

"""
Function: print_dpTable

Inputs: dpTable

Description:
This function prints the input dpTable
"""
def print_dpTable(dpTable):

	for i in range(0, len(dpTable)):
		for j in range(0, len(dpTable[i])):
			print("(", i, ", ", j, ") -> ", dpTable[i][j])


################################################################################

"""
Main function.
"""
if __name__ == "__main__":
    edTests(False)
    print()
    compareGenomes(True, 30, 300, 'ED')
    print()
    compareRandStrings(True, 30, 300, 'ED')
    print()
    compareGenomes(True, 30, 300, 'ASM')
    print()
    compareRandStrings(True, 30, 300, 'ASM')
